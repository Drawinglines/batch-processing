# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 16:10:27 2016
CVMCP&kVqSR

chdkcE?iU(Z
@author: Administrator
"""

import boto3
import time

client = boto3.client('ec2')
ec2 = boto3.resource('ec2')

current_status = client.describe_spot_instance_requests()

for i in xrange(len(current_status["SpotInstanceRequests"])):
    if current_status["SpotInstanceRequests"][i]["Status"]["Code"] != "fulfilled":
        print current_status["SpotInstanceRequests"][i]["SpotInstanceRequestId"],\
                current_status["SpotInstanceRequests"][i]["Status"]["Code"]
    else:
        print current_status["SpotInstanceRequests"][i]["InstanceId"],\
          current_status["SpotInstanceRequests"][i]["Status"]["Code"]

#if a == "marked-for-termination":


spot = ec2.Instance('i-92d6738a')
res = ec2.Instance('i-86e65a13')

res_vol_id = res.block_device_mappings[0]["Ebs"]["VolumeId"]
spot_vol_id = spot.block_device_mappings[0]["Ebs"]["VolumeId"]
spot_name = spot.block_device_mappings[0]["DeviceName"]

#res_vol_status = res.detach_volume(VolumeId=res_vol)
#spot_vol_status = spot.detach_volume(VolumeId=spot_vol)
#
#res.attach_volume(VolumeId=spot_vol, Device=spot_name)

snapshot = ec2.create_snapshot(VolumeId="vol-5315f7db", Description="testing spot instances")
#snapshot = ec2.Snapshot("snap-e39a0fcd")
volume = ec2.create_volume(SnapshotId="snap-e39a0fcd", AvailabilityZone='us-west-2a')
res.attach_volume(VolumeId="vol-5315f7db", Device='/dev/sdy') #will be mapped to /xvdy
snapshot.delete()


b = client.describe_instances(InstanceIds=['i-92d6738a'])
spot_public_dns = b["Reservations"][0]["Instances"][0]["PublicDnsName"]