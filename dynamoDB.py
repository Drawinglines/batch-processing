# -*- coding: utf-8 -*-
"""
Created on Tue Aug 02 14:44:26 2016
Python & DynamoDB
@author: Administrator
"""
from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")

""" table = dynamodb.create_table(
    TableName='Movies',
    KeySchema=[
        {
            'AttributeName': 'year',
            'KeyType': 'HASH'  #Partition key
        },
        {
            'AttributeName': 'title',
            'KeyType': 'RANGE'  #Sort key
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'year',
            'AttributeType': 'N'
        },
        {
            'AttributeName': 'title',
            'AttributeType': 'S'
        },

    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
)

print("Table status:", table.table_status) """


table = dynamodb.Table('Movies')

with open ("D:\moviedata.json") as json_file:
    movies = json.load(json_file, parse_float = decimal.Decimal)
    for movie in movies[0:10]:
        year = int(movie['year'])+10
        title = movie['title']+"_copy"
        info = movie['info']

        a = {
               'year': year,
               'title': title,
               'info': info
            }
        print("Adding movie:", year, title)

        table.put_item(
           Item = a
        )