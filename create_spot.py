# -*- coding: utf-8 -*-
"""
Created on Tue Sep 06 11:47:37 2016
Create spot instances
@author: Administrator
"""

import boto3
import datetime
client = boto3.client('ec2')
response = client.request_spot_instances(
    DryRun=False,
    SpotPrice='0.20',
    ClientToken='6b',
    InstanceCount=1,
    Type='one-time',
    LaunchSpecification={
        'ImageId': "ami-34b24954",
        'KeyName': 'allsoftwarekey',
        'SecurityGroups': ["default"],
        'InstanceType': 'c3.large',
        'Placement': {
            'AvailabilityZone': 'us-west-2a',
        },
        'BlockDeviceMappings': [
            {
                'DeviceName': 'xvdh',
                'Ebs': {
                    'SnapshotId': 'snap-e39a0fcd',
                    'VolumeSize': 30,
                    'DeleteOnTermination': True,
                    'VolumeType': 'gp2',
                    'Encrypted': False
                },
            },
        ],

        'EbsOptimized': True,
        'Monitoring': {
            'Enabled': False
        },
        'SecurityGroupIds': [
            'sg-b51907d0',
        ]
        
    }
)

print response